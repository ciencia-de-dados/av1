# Projeto AV1 


Você foi contratado como cientista de dados pela prefeitura da cidade XPTO. A prefeitura por meio
da coleta de dados de seus sistemas, possui alguns conjuntos de dados e gostaria de explorá-los para
apoiar as tomadas de decisão do prefeito e seus secretários. A base de dados da prefeitura conta com
dados sobre as áreas da Saúde (dados de cidadãos que tiverem dengue), Mobilidade (dados de
cidadãos que andam de ônibus) e Educação (dados de cidadãos matriculados em escolas), essas
informações estão em tês arquivos Excel (.csv). Como cientista de dados você deverá fazer:

- [ ] Projeto
- - [ ] Higienização das bases de dados
- - - [ ] Aluno
- - - [ ] Dengue
- - - [ ] Ônibus
- - [ ] Padronização das bases de dados
- - - [ ] Aluno
- - - [ ] Dengue
- - - [ ] Ônibus
- - [ ] Parear as bases de dados
- - - [ ] Aluno
- - - [ ] Dengue
- - - [ ] Ônibus
- - [ ] Apresentar os dados tratados em uma plataforma de Business Intelligence



### Base de Dados - Aluno
<details><summary>Atributos</summary>
<br>
<p>
- ID <br>
- NOME <br>
- NOME DA MAE <br>
- NOME DO PAI <br>
- SEXO <br>
- DATA DE NASCIMENTO <br>
- BAIRRO
</p>
</details>

### Base de Dados - Dengue

<details><summary>Atributos</summary>
<br>
<p>
- ID <br>
- NOME <br>
- NOME DA MAE <br>
- NOME DO PAI <br>
- SEXO <br>
- DATA DE NASCIMENTO <br>
- BAIRRO <br>
- DATA DA DENGUE <br>
</p>
</details>



### Base de Dados - Onibus

<details><summary>Atributos</summary>
<br>
<p>
- ID <br>
- NOME <br>
- NOME DA MAE <br>
- NOME DO PAI <br>
- SEXO <br>
- DATA DE NASCIMENTO <br>
- BAIRRO <br>
- ÔNIBUS <br>
</p>
</details>


## Padronização e Higienização dos Dados


- [x] Inverter NOME DA MAE e NOME DO PAI em todas as tabelas
- [x] Padronizar a coluna SEXO em todas as tabelas para M (Masculino) E F (Feminino)
- [x] Padronizar as colunas referentes a DATA em todas as tabelas, colocando os zeros corretamente e retirando os caracteres especiais.


## Links úteis

[Dados de Bairros Regionais](http://dados.fortaleza.ce.gov.br/catalogo/dataset/bairros-regionaibs) <br>
[Limite Bairros](http://dados.fortaleza.ce.gov.br/catalogo/dataset/limite-bairros) <br>
[Nome bairros de fortaleza](http://dados.fortaleza.ce.gov.br/catalogo/dataset/desenvolvimento-humano-por-bairro-de-fortaleza)





